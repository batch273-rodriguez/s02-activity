/*
    Activity - Quiz

    1. Spaghetti code
    2. let objectName = {key:value,method()}
    3. Encapsulation
    4. studentOne.enroll()
    5. True
    6. key: value
    7. True
    8. True
    9: True
    10. True


*/

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89,84,78,88],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach((grade)=>{sum=sum+grade});
        return sum / this.grades.length;
    },
    willPass() {
        if (this.computeAve()>=85)
        {return true}
        else 
        {return false}
    },
    willPassWithHonors() {
        if (this.computeAve()>=90) { return true }
        if (this.computeAve()>=85&&this.computeAve<90) { return false}
        if (this.computeAve()<85) { return undefined }
    }
}

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach((grade)=>{sum=sum+grade});
        return sum / this.grades.length;
    },
    willPass() {
        if (this.computeAve()>=85)
        {return true}
        else 
        {return false}
    },
    willPassWithHonors() {
        if (this.computeAve()>=90) { return true }
        if (this.computeAve()>=85&&this.computeAve<90) { return false}
        if (this.computeAve()<85) { return undefined }
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach((grade)=>{sum=sum+grade});
        return sum / this.grades.length;
    },
    willPass() {
        if (this.computeAve()>=85)
        {return true}
        else 
        {return false}
    },
    willPassWithHonors() {
        if (this.computeAve()>=90) { return true }
        if (this.computeAve()>=85&&this.computeAve<90) { return false}
        if (this.computeAve()<85) { return undefined }
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0; 
        this.grades.forEach((grade)=>{sum=sum+grade});
        return sum / this.grades.length;
    },
    willPass() {
        if (this.computeAve()>=85)
        {return true}
        else 
        {return false}
    },
    willPassWithHonors() {
        if (this.computeAve()>=90) { return true }
        if (this.computeAve()>=85&&this.computeAve<90) { return false}
        if (this.computeAve()<85) { return undefined }
    }
}

// console.log(studentOne);
// console.log(studentTwo);
// console.log(studentThree);
// console.log(studentFour);

let classOf1A = {
    students: [studentOne,studentTwo,studentThree,studentFour],
    countHonorStudents() {
        let countPassed = 0;
        this.students.forEach((student)=>{ if(student.willPassWithHonors()) {countPassed++} });
        return countPassed;
    },
    honorsPercentage() {
        return 100*this.countHonorStudents()/this.students.length;
    },
    retrieveHonorStudentInfo() {
        let arrHonorStudents = [];
        this.students.forEach((student)=>{if(student.willPassWithHonors()){arrHonorStudents.push(student)}});
        return arrHonorStudents;
    }
    
};

console.log(classOf1A);